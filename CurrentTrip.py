class CurrentTrip:
    def __init__(self, departureDate, departureTime, departureName, departureType, destinationId, destinationDate, destinationTime, destinationName):
        self.departureDate = departureDate
        self.departureTime = departureTime
        self.departureName = departureName
        self.departureType = departureType
        self.destinationId = destinationId
        self.destinationDate = destinationDate
        self.destinationTime = destinationTime
        self.destinationName = destinationName

    def printTrip(self):
        print('{} {} {} {}'.format(self.departureDate, self.departureTime, self.departureName, self.departureType))
        print('{} {} {}'.format(self.destinationDate, self.destinationTime, self.destinationName))