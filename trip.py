import requests
import CurrentTrip as CT

# Build depature-url and request json
def getDepatureJson(apiKey, departureName):
    depatureURL = 'https://api.resrobot.se/v2/location.name?key={apiKey}&input={departureName}&format=json'.format(apiKey=apiKey, departureName=departureName)
    return requests.get(depatureURL).json()

# If StopLocation > 1 return a list with all options
def getDepatureOptions(departureJson):
    departureNames = []
    counter = len(departureJson['StopLocation'])
    for i in range(counter):
        departureNames.append(departureJson['StopLocation'][i]['name'])
    return departureNames

def getDestinationJson(apiKey, destinationName):
    destinationURL = 'https://api.resrobot.se/v2/location.name?key={apiKey}&input={destinationName}&format=json'.format(apiKey=apiKey, destinationName=destinationName)
    return requests.get(destinationURL).json()

def getDestinationOptions(destinationJson):
    destinationNames = []
    counter = len(destinationJson['StopLocation'])
    for i in range(counter):
        destinationNames.append(destinationJson['StopLocation'][i]['name'])
    return destinationNames

def getTripJson(apiKey, departureId, destinationId, time):
    tripURL = 'https://api.resrobot.se/v2/trip?key={apiKey}&originId={departureId}&destId={destinationId}&format=json&time={time}'.format(apiKey=apiKey, departureId=departureId, destinationId=destinationId, time=time)
    return requests.get(tripURL).json()

def createTripObject(tripJson, i):
    departureDate = tripJson['Trip'][i]['LegList']['Leg'][0]['Origin']['date'] # depature date
    departureTime = tripJson['Trip'][i]['LegList']['Leg'][0]['Origin']['time'] # depature time
    departureName = tripJson['Trip'][i]['LegList']['Leg'][0]['Origin']['name'] # depature name
    departureType = tripJson['Trip'][i]['LegList']['Leg'][0]['Product']['name'] # type

    test = len(tripJson['Trip'][i]['LegList']['Leg'])-1
    destinationId = tripJson['Trip'][i]['LegList']['Leg'][test]['Destination']['id']
    destinationDate = tripJson['Trip'][i]['LegList']['Leg'][test]['Destination']['date']
    destinationTime = tripJson['Trip'][i]['LegList']['Leg'][test]['Destination']['time']
    destinationName = tripJson['Trip'][i]['LegList']['Leg'][test]['Destination']['name']

    return CT.CurrentTrip(departureDate, departureTime, departureName, departureType, destinationId, destinationDate, destinationTime, destinationName)