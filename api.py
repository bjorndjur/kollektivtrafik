from os.path import expanduser
import json

def getKey(api):
    home = expanduser("~")
    path = home + '/apikey.json'
    with open(path, "r") as file:
        key = file.read()
    fileJson = json.loads(key)
    return fileJson['keys'][0][api]