import sys
import api
import trip

departureName = sys.argv[1]
destinationName = sys.argv[2]
time = sys.argv[3]

apiKey = api.getKey('ResRobot')
departureJson = trip.getDepatureJson(apiKey,departureName)
destinationJson = trip.getDestinationJson(apiKey,destinationName)

if (len(departureJson['StopLocation']) > 1):
    departureNames = trip.getDepatureOptions(departureJson)
    print('Results for {}'.format(departureName))
    for i in range(len(departureNames)):
        print('{}: {}'.format(i, departureNames[i]))

    userDeparture = input('[0-{}] > '.format(len(departureJson['StopLocation'])-1)) 
    departureId = departureJson['StopLocation'][int(userDeparture)]['id']


if (len(destinationJson['StopLocation']) > 1):
    destinationNames = trip.getDestinationOptions(destinationJson)
    print('Results for {}'.format(destinationName))
    for i in range(len(destinationNames)):
        print('{}: {}'.format(i, destinationNames[i]))
    userDestination = input('[0-{}] > '.format(len(destinationJson['StopLocation'])-1)) 
    destinationId = destinationJson['StopLocation'][int(userDestination)]['id']

tripJson = trip.getTripJson(apiKey, departureId, destinationId, time)

tripList = []
# temp fix with -1 
for i in range(len(tripJson) -1):
    tripList.append(trip.createTripObject(tripJson, i))
for i in range(len(tripList)):
    print('-' * 3)
    print('Option {}'.format(i))
    tripList[i].printTrip()