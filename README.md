# Kollektivtrafik is a Python script that generates timetables for Swedish public transport.

## Syntax 
python3 kollektivtrafik.py depatureName destinationName HH:MM
